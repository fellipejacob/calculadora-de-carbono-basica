// class Values {
//     constructor(
//         auto_number_people, auto_gasoline_month, auto_ethanol_month,
//         auto_diesel_month, auto_gnv_month, plane_br_trips,
//         plane_int_trips, home_eletric_number_people, home_eletric_consuming,
//         fuel_glp_duration, fuel_gas_consumption, agro_bovine_consuming,
//         agro_chicken_consuming, agro_swine_consuming, agro_milk_consuming,
//         agro_eggs_consuming, recycling_bool, country_state, family_income_month,
//         gasolineConsumption, hydratedAlcoholConsumption, dieselConsumption,
//         gnvConsumption, domesticTrip, internationalTrip
//     ) {
//         this.country_state = document.getElementById("country_state").value;
//         this.family_income_month = document.getElementById("family_income_month").value;
//         this.auto_number_people = document.getElementById("auto_number_people").value;
//         this.auto_gasoline_month = document.getElementById("auto_gasoline_month").value;
//         this.auto_ethanol_month = document.getElementById("auto_ethanol_month").value;
//         this.auto_diesel_month = document.getElementById("auto_diesel_month").value;
//         this.auto_gnv_month = document.getElementById("auto_gnv_month").value;
//         this.plane_br_trips = document.getElementById("plane_br_trips").value;
//         this.plane_int_trips = document.getElementById("plane_int_trips").value;
//         this.home_eletric_number_people = document.getElementById("home_eletric_number_people").value;
//         this.home_eletric_consuming = document.getElementById("home_eletric_consuming").value;
//         this.fuel_glp_duration = document.getElementById("fuel_glp_duration").value;
//         this.fuel_gas_consumption = document.getElementById("fuel_gas_consumption").value;
//         this.agro_bovine_consuming = document.getElementById("agro_bovine_consuming").value;
//         this.agro_chicken_consuming = document.getElementById("agro_chicken_consuming").value;
//         this.agro_swine_consuming = document.getElementById("agro_swine_consuming").value;
//         this.agro_milk_consuming = document.getElementById("agro_milk_consuming").value;
//         this.agro_eggs_consuming = document.getElementById("agro_eggs_consuming").value;
//         this.recycling_bool = false;

//         // constants
//         this.cGasolineConsumption = 1.62609244969273;
//         this.hydratedAlcoholConsumption = 0;
//         this.dieselConsumption = 2.33564547092433;
//         this.gnvConsumption = 2.066939424;
//         this.domesticTrip = 106.053647266232;
//         this.internationalTrip = 605.565693173648;
//     }

//     getInitialValueInput() {
//         console.log(this.country_state);
//         console.log(this.family_income_month);
//         console.log(this.auto_number_people);
//         console.log(this.auto_gasoline_month);
//         console.log(this.auto_ethanol_month);
//         console.log(this.auto_diesel_month);
//         console.log(this.auto_gnv_month);
//         console.log(this.plane_br_trips);
//         console.log(this.plane_int_trips);
//         console.log(this.home_eletric_number_people);
//         console.log(this.home_eletric_consuming);
//         console.log(this.fuel_glp_duration);
//         console.log(this.fuel_gas_consumption);
//         console.log(this.agro_bovine_consuming);
//         console.log(this.agro_chicken_consuming);
//         console.log(this.agro_swine_consuming);
//         console.log(this.agro_milk_consuming);
//         console.log(this.agro_eggs_consuming);
//         console.log(this.recycling_bool);
//     }

//     getValueByCountry(country) {
//         let countryValue = 0;
//         switch (country) {
//             case 'AC':
//                 countryValue = 38.9073861859024;
//                 break;
//             case 'AL':
//                 countryValue = 37.2383278505457;
//                 break;
//             case 'AM':
//                 countryValue = 39.6514223427577;
//                 break;
//             case 'AP':
//                 countryValue = 40.9529213601668;
//                 break;
//             case 'BA':
//                 countryValue = 75.0268673497524;
//                 break;
//             case 'CE':
//                 countryValue = 39.6026264316023;
//                 break;
//             case 'DF':
//                 countryValue = 44.6627442538841;
//                 break;
//             case 'ES':
//                 countryValue = 88.8400643700606;
//                 break;
//             case 'GO':
//                 countryValue = 140.6305460397540;
//                 break;
//             case 'MA':
//                 countryValue = 51.5927523470594;
//                 break;
//             case 'MG':
//                 countryValue = 51.5927523470594;
//                 break;
//             case 'MS':
//                 countryValue = 111.5215909117040;
//                 break;
//             case 'MT':
//                 countryValue = 173.2814406650770;
//                 break;
//             case 'PA':
//                 countryValue = 292.7582370874210;
//                 break;
//             case 'PB':
//                 countryValue = 91.7470288559590;
//                 break;
//             case 'PE':
//                 countryValue = 38.7672560353184;
//                 break;
//             case 'PI':
//                 countryValue = 51.6370722819295;
//                 break;
//             case 'PR':
//                 countryValue = 58.6501354373074;
//                 break;
//             case 'RJ':
//                 countryValue = 172.8509054392570;
//                 break;
//             case 'RN':
//                 countryValue = 46.5931599041435;
//                 break;
//             case 'RO':
//                 countryValue = 45.1132399129702;
//                 break;
//             case 'RR':
//                 countryValue = 161.1569462389510;
//                 break;
//             case 'RS':
//                 countryValue = 50.9930966507428;
//                 break;
//             case 'SC':
//                 countryValue = 115.3295197917270;
//                 break;
//             case 'SE':
//                 countryValue = 130.9962084723480;
//                 break;
//             case 'SP':
//                 countryValue = 49.2717164671729;
//                 break;
//             case 'TO':
//                 countryValue = 236.6247782348000
//                 break;
//             default:
//                 break;
//         }
//         return countryValue;
//     }

//     getEmissionsInPassengerTransportation() {
//         let sBs4 = this.country_state;
//         let sBs10 = this.auto_number_people;
//         let sBs11 = this.auto_gasoline_month;
//         let sBs12 = this.auto_ethanol_month;
//         let sBs13 = this.auto_diesel_month;
//         let sBs14 = this.auto_gnv_month;
//         let sBs16 = this.plane_br_trips;
//         let sBs17 = this.plane_int_trips;
//         let countryValue = this.getValueByCountry(sBs4);

//         // 'ENERGIA E INDÚSTRIA'!$D$3:
//         let energyIndustrysDs3 = this.cGasolineConsumption;
//         let energyIndustrysDs4 = this.hydratedAlcoholConsumption;
//         let energyIndustrysDs5 = this.dieselConsumption;
//         let energyIndustrysDs6 = this.gnvConsumption;
//         let energyIndustrysDs7 = this.domesticTrip;
//         let energyIndustrysDs8 = this.internationalTrip;
//         let response_1 = sBs10 > 0 ? 12 * (1 / sBs10) * (1 / 1000) * sBs11 * energyIndustrysDs3 : 0;
//         let response_2 = sBs10 > 0 ? 12 * (1 / sBs10) * (1 / 1000) * sBs12 * energyIndustrysDs4 : 0;
//         let response_3 = sBs10 > 0 ? 12 * (1 / sBs10) * (1 / 1000) * sBs13 * energyIndustrysDs5 : 0;
//         let response_4 = sBs10 > 0 ? 12 * (1 / sBs10) * (1 / 1000) * sBs14 * energyIndustrysDs6 : 0;
//         let response_5 = sBs16 * energyIndustrysDs7;
//         let response_6 = (1 / 1000) * sBs17 * energyIndustrysDs8 + (1 / 1000) * countryValue;
//         let response = response_1 + response_2 + response_3 + response_4 + response_5 + response_6;

//         return response;
//     }

//     calculateResults() {

//         this.country_state = document.getElementById("country_state").value;
//         this.family_income_month = document.getElementById("family_income_month").value;
//         this.auto_number_people = document.getElementById("auto_number_people").value;
//         this.auto_gasoline_month = document.getElementById("auto_gasoline_month").value;
//         this.auto_ethanol_month = document.getElementById("auto_ethanol_month").value;
//         this.auto_diesel_month = document.getElementById("auto_diesel_month").value;
//         this.auto_gnv_month = document.getElementById("auto_gnv_month").value;
//         this.plane_br_trips = document.getElementById("plane_br_trips").value;
//         this.plane_int_trips = document.getElementById("plane_int_trips").value;
//         this.home_eletric_number_people = document.getElementById("home_eletric_number_people").value;
//         this.home_eletric_consuming = document.getElementById("home_eletric_consuming").value;
//         this.fuel_glp_duration = document.getElementById("fuel_glp_duration").value;
//         this.fuel_gas_consumption = document.getElementById("fuel_gas_consumption").value;
//         this.agro_bovine_consuming = document.getElementById("agro_bovine_consuming").value;
//         this.agro_chicken_consuming = document.getElementById("agro_chicken_consuming").value;
//         this.agro_swine_consuming = document.getElementById("agro_swine_consuming").value;
//         this.agro_milk_consuming = document.getElementById("agro_milk_consuming").value;
//         this.agro_eggs_consuming = document.getElementById("agro_eggs_consuming").value;

//         let emissionsInPassengerTransportation = this.getEmissionsInPassengerTransportation();
//         document.getElementById('response_emissions_in_passenger_transportation').innerHTML = emissionsInPassengerTransportation;
//         // console.log('emissionsInPassengerTransportation: ', emissionsInPassengerTransportation);
//         // console.log(this.country_state);
//         // console.log(this.family_income_month);
//         // console.log(this.auto_number_people);
//         // console.log(this.auto_gasoline_month);
//         // console.log(this.auto_ethanol_month);
//         // console.log(this.auto_diesel_month);
//         // console.log(this.auto_gnv_month);
//         // console.log(this.plane_br_trips);
//         // console.log(this.plane_int_trips);
//         // console.log(this.home_eletric_number_people);
//         // console.log(this.home_eletric_consuming);
//         // console.log(this.fuel_glp_duration);
//         // console.log(this.fuel_gas_consumption);
//         // console.log(this.agro_bovine_consuming);
//         // console.log(this.agro_chicken_consuming);
//         // console.log(this.agro_swine_consuming);
//         // console.log(this.agro_milk_consuming);
//         // console.log(this.agro_eggs_consuming);
//         // console.log(this.recycling_bool);

//     }
// }

